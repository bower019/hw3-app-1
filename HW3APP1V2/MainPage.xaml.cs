﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;

namespace HW3APP1V2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

        }
        async void OnTreesTapped(object sender, System.EventArgs e)
        {
            bool usersResponse = await DisplayAlert("Awesome I like trees too. Ready to see them?",
                                "Ready to see them??",
                                "Yes Please!",
                                "I want to make firewood");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new TreePage());
            }
        }

        void OnWolvesTapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new WolvesPage("Here is a wolf"));
        }

        async void OnTapped(object sender, System.EventArgs e)
        {

            string response = await DisplayActionSheet("Overloaded Action Sheet",
                                  "Cancel",
                                  null,
                                  "Canines",
                                  "Felines",
                                  "Avian",
                                  "Reptilian");
            Debug.WriteLine($"User picked:  {response}");

            if (response.Equals("Canines", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new WolvesPage("Here is a wolf"));
            }


        }




    }
}