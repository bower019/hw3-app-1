﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


using Xamarin.Forms;

namespace HW3APP1V2
{
    public partial class WolvesPage : ContentPage
    {
        public WolvesPage(String message)
        {
            InitializeComponent();
            messageLabel.Text = message;
        }
    }
}
